Tyler Moffitt is a family law and criminal defense attorney in LaGrange. Moffitt Law, LLC is an authority on family law and criminal law in Georgia. Attorney Tyler Moffitt handles prenups, child support, divorce, child custody, DUI's, sex crime defense and domestic violence defense.

Address: 309 Old Morgan Street, LaGrange, GA 30240, USA

Phone: 762-200-2495